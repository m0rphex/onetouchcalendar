package oop.team6.Calendar.Classes;

import java.util.Date;

/**
 *
 * @author morph
 */
public abstract class Event {

    private Date date;
    private String description;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

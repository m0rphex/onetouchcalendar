package oop.team6.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Calendar;

public class MainActivity extends Activity {
//public final static String EXTRA_TIME = "oop.team6.Calendar.time";

    /**
     
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        //here we can add code that is ran when the app starts
        //all the activities have this (its a good place to retrieve out json/objects ;)
        super.onCreate(savedInstanceState); //this is a native method for retrieving earlier states
        setContentView(R.layout.main);//this gets our xml layout!
        
        
        
        //Im testing some object creation and parsing to JSON
        TestEntity example = new TestEntity();
        example.setName("An Droid");

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

       // String flat = gson.toJson(example);
       // ((EditText) findViewById(R.id.testgson)).setText(flat);

    }
//This method is used to launch the CalendarActivity :)

    public void openCalendarView(View view) {
        //we can/should create an intent object, this object can be retrieved in the next activity
        //we can also add data/info to be passed to the next activity
        Intent myIntent = new Intent(MainActivity.this, CalendarActivity.class);
        //myIntent.putExtra("key", value); //Optional parameters
        MainActivity.this.startActivity(myIntent); //this starts the next activity
        //when we start the next activity, this (Main) activity is paused 
    }

    public void openTimetableView(View view) {
        Intent myIntent = new Intent(this, TimetableActivity.class); //we can just use "this"
        Calendar dateNow = Calendar.getInstance();
        myIntent.putExtra("time", dateNow.getTime().toString());
        MainActivity.this.startActivity(myIntent);
    }
    

}
